// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
{
    functor(T x) : _x(x) {}
	void operator()(T& e) const { e += _x; }
private:
	T _x;
};


template <typename T>
struct predicate
{
	bool operator()(T& e) const {
		if (e < 0 ) {
			return true;
		}
		else {
			return false;
		}
	}
};
template <typename T>
struct printer
{
public:
    void operator()(T& e) const {std::cout << e << std::endl;}
};

struct input_reader 
{
	input_reader() {};

	std::vector<int> operator()(const char* path)
	{
		std::ifstream fin(path);
		std::string line;
		std::vector<int> v;

		while(!fin.eof())
		{
			fin >> line;
			v.push_back(std::atoi(line.c_str()));
			//cout << v << endl;
		}
		fin.close();
		return v;
	};
};

struct output_writer
{
	output_writer() {};
	void operator()(const char* path, std::vector<int>& data)
	{
		std::ofstream fout(path);

		if (fout.is_open())
		{
			for (int i = 0; i < data.size(); i++) {
				fout << data[i] << std::endl;
			}
		}
		fout.close();
	};
	// not implemented
};
int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<int> source_v;
    typedef int curr_type;
    std::vector<curr_type> target_v;

	input_reader lexa;
	source_v = lexa(INPUT_FILE);

    predicate<curr_type> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<curr_type> func(10);
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<curr_type> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer write;
    write(OUTPUT_FILE, target_v);

    return 0;
}
